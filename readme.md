# cable simulator for YouMo

## convert pdf to svg

https://svgconverter.com/pdf-to-svg

### group

The folowing group should be possible to enable and disable

* Motor
* Akku
* Display
* Connector
* Info

## webpage

There should be a webpage to show all the different harnesses. 
Select a harness and it's configuration with simple interface.

### svg load

(A) every harness has it's corresponding file

(B) one svg file for all harnesses

## docu

```mermaid
sequenceDiagram
    participant HTML as HTML
    participant Browser as Browser
    participant Server as Server
    participant SVG File as SVG

    HTML ->> Browser: Request HTML page
    Browser ->> Server: Request SVG file
    Server ->> SVG File: Retrieve SVG file
    SVG File -->> Server: Send SVG file
    Server -->> Browser: Send SVG file
    Browser -->> HTML: Receive SVG file
    HTML -->> Browser: Render SVG
```

## https-server

> $ npm install -g http-server

> $ http-server